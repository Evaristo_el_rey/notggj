using UnityEngine;
using System.Collections;

public class DeathController : MonoBehaviour {


	public string 			m_ResetSceneName 		= "";				//The name of the scene to load after the death animation is reproduced.
	public string 			m_DeathAnimationName 	= "";				//The name of the death scene to reproduce.
	public float			m_TimeDieing 			= 0.0f;				//The time that will pass between the time of dieing and the reseting of the scene.
	
	private bool 			m_Dieing 				= false;			//A Flag specifying if the dying proces has started.
	private float			m_DieingStartTime 		= 0.0f;				//The time at which the player started dieing.
	private GameObject 		m_Player 				= null;				//The reference to the player.
	
	public void Die()
	{
		m_Dieing = true;
		m_DieingStartTime = Time.time;
		//PENDING: LAUNCH DIEING ANIMATION!
		if(m_Player == null){
			print ("WARNING:DEATHCONTROLLER: Player set.");
		}
		
		Animation animation = m_Player.GetComponent<Animation>();
		if( animation == null ){
			print ("WARNING:DEATHCONROLLER: Player does not have an Animation Controller");
			return;
		}
		animation.CrossFade(m_DeathAnimationName);
	}
	
	// Use this for initialization
	void Start () {
		m_Player = this.gameObject;
		if( gameObject.tag != "Player" )
		{
			print ("WARNING: DeathController not attached to a player");
		}
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if( m_Dieing )
		{
			if(m_TimeDieing <= Time.time - m_DieingStartTime)	
			{
				ResetScene ();
			}
		}
	}
	
	private void ResetScene(){
		Application.LoadLevel(m_ResetSceneName);	
	}
}
