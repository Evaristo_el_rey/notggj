using UnityEngine;
using System.Collections;

public class InterfaceGUI : MonoBehaviour {
	public float RealMaximum;

	public GameObject fondo;
	public GameObject RellenoEscalable;
	
	public GameObject RellenoCompletoCollapsed;
	public GameObject RellenoCompletoBerserker;
	public GameObject RellenoCompletoExcited;
	public GameObject RellenoCompletoNeutral;
	public GameObject RellenoCompletoRelaxed;
	public GameObject RellenoCompletoZombie;
	public GameObject RellenoCompletoFaint;
	// Use this for initialization
	
	protected Vector3 InitialPos;
	
	protected State state;
	float GetMaxBPM() { return state.m_MaximumLevel; }
	float GetMinBPM() { return state.m_MinimumLevel; }
	float GetActualBPM() { return state.GetCurrentBPMLevel(); }
	
	//Desactivamos los rellenos. Cambiamos las escalas iniciales.
	void Start () {
		//t = GameObject.Find("singleton").GetComponent<TimeWarp>();
		state = GameObject.Find("singleton").GetComponent<State>();
	    fondo.renderer.enabled =true;
		RellenoCompletoExcited.renderer.enabled = false;
		RellenoCompletoBerserker.renderer.enabled = false;
		RellenoCompletoZombie.renderer.enabled = false;
		RellenoCompletoFaint.renderer.enabled = false;
		
		InitialPos = RellenoEscalable.transform.localPosition;
	}
	
	void SetScale(GameObject go, float scl, Vector3 InitialPos) {
		Vector3 newScale = new Vector3(scl,1,1);
		go.transform.localScale = newScale;
		go.transform.localPosition = InitialPos + newScale/2.0f;
	}
	// Cambiamos la escala. Activamos/desactivamos si es necesario.
	void Update () {
		//Activo/desactivo los completos
		float bpm = GetActualBPM ();
		RellenoCompletoFaint.renderer.enabled     = bpm>state.m_MinimumLevel;
		RellenoCompletoZombie.renderer.enabled    = bpm>state.m_FaintToZombieLevel;
		RellenoCompletoRelaxed.renderer.enabled   = bpm>state.m_ZombieToRelaxedLevel;
		RellenoCompletoNeutral.renderer.enabled   = bpm>state.m_RelaxedToNeutralLevel;
		RellenoCompletoExcited.renderer.enabled   = bpm>state.m_NeutralToExcitedLevel;
		RellenoCompletoBerserker.renderer.enabled = bpm>state.m_ExcitedToBersekerLevel;
		RellenoCompletoCollapsed.renderer.enabled = bpm>state.m_BersekerToCollapsedLevel;
		//Escalo los parciales.
		SetScale(RellenoEscalable, bpm/100.0f * RealMaximum, InitialPos);
	}
}
