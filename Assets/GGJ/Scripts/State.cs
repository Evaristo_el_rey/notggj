using UnityEngine;
using System.Collections;

public class State : MonoBehaviour {

	public enum BPMLevel {
		Faint,					//Faint: The Player faints due to the low BPM level.
		Zombie,					//Zombie: The player is in a zombie state where he has psy powers.
		Relaxed,				//Relaxed: The player feels so relaxed that his perception of the time is altered.
		Neutral,
		Excited,				//Excited: The player feels so excited that his perception of the time is altered.
		Berseker,				//Berseker: The player has extra physical habilities.
		Collapsed				//Collapsed: The player collapses.
	};

	public 		float 		m_IncreasePerSecondPercentage		= 1.0f;			//The percentage to increase per second.	

	public		float 		m_MinimumLevel						= 0.0f;
	public 		float 		m_FaintToZombieLevel 				= 10.0f;			
	public		float		m_ZombieToRelaxedLevel				= 25.0f;		
	public		float		m_RelaxedToNeutralLevel				= 40.0f;		
	public		float		m_NeutralToExcitedLevel				= 60.0f;		
	public		float		m_ExcitedToBersekerLevel			= 75.0f;		 
	public		float		m_BersekerToCollapsedLevel			= 90.0f;		 
	public		float		m_MaximumLevel						= 100.0f;
	public      float       m_ResetLevel						= 50.0f;
	
	private		float		m_CurrentBPMLevel 					= 50.0f;		//The current BPM level.	
	
	private 	TimeWarp 	m_TimeWarp							= null;
	
	
	public float 	 GetCurrentBPMLevel()
	{
		return m_CurrentBPMLevel;
	}
	
	public BPMLevel GetCurrentBPMLevelType()
	{
		if( m_CurrentBPMLevel < m_FaintToZombieLevel ) return BPMLevel.Faint;	
		if( m_CurrentBPMLevel < m_ZombieToRelaxedLevel ) return BPMLevel.Zombie;	
		if( m_CurrentBPMLevel < m_RelaxedToNeutralLevel ) return BPMLevel.Relaxed;	
		if( m_CurrentBPMLevel < m_NeutralToExcitedLevel ) return BPMLevel.Neutral;	
		if( m_CurrentBPMLevel < m_ExcitedToBersekerLevel ) return BPMLevel.Excited;	
		if( m_CurrentBPMLevel < m_BersekerToCollapsedLevel ) return BPMLevel.Berseker;	
		return BPMLevel.Berseker;
	}
		
	public void SetIncreasePerSecondPercentage( float p )
	{
		m_IncreasePerSecondPercentage = p;
	}
	
	public void Reset()
	{
		m_CurrentBPMLevel = m_ResetLevel;
	}
	
	// Use this for initialization
	void Start () {	
	
		m_TimeWarp = GameObject.Find("singleton").GetComponent<TimeWarp>();
	
	}
	
	// Update is called once per frame
	void Update () {
		m_CurrentBPMLevel += m_IncreasePerSecondPercentage * Time.deltaTime;
		if( m_CurrentBPMLevel > m_MaximumLevel ) m_CurrentBPMLevel = m_MaximumLevel;
		if( m_CurrentBPMLevel < m_MinimumLevel ) m_CurrentBPMLevel = m_MinimumLevel;
	}
}
