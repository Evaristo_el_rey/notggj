using UnityEngine;
using System.Collections;

public class TorqueMovement : MonoBehaviour 
{
	public float mMaxAngularVelocity = 1.0f;
	private float mMultiplier = 1.0f;
	// Use this for initialization
	void Start () 
	{
		rigidbody.maxAngularVelocity = mMaxAngularVelocity;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		mMultiplier =  GameObject.FindGameObjectWithTag("TimeWarp").GetComponent<TimeWarp>().getWorldMult();
		rigidbody.maxAngularVelocity = mMaxAngularVelocity * mMultiplier;
	}
}
