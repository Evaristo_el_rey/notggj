using UnityEngine;
using System.Collections;

public class LateralMovement : MonoBehaviour 
{
	public float mVelocity = 10.0f;
	public float mRange = 10.0f;
	private float mCenter;
	private float mMultiplier;
	
	private int mDirection = 1; //1 +x -1 -> -x
	private float mMaxPosition;
	private float mMinPosition;
	// Use this for initialization
	void Start () 
	{
		mCenter = gameObject.transform.position.x;
		mMaxPosition = mCenter + mRange;
		mMinPosition = mCenter - mRange;
		mMultiplier = 1.0f;
		
	}	
	
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		float x = gameObject.transform.position.x;
		/*GameObject timeWarp = GameObject.FindGameObjectWithTag("TimeWarp");
		mMultiplier =  timeWarp.GetComponent<TimeWarp>().getWorldMult();*/
//		mMultiplier =  GameObject.FindGameObjectWithTag("TimeWarp").GetComponent<TimeWarp>().getWorldMult();
		mMultiplier =  GameObject.Find("singleton").GetComponent<TimeWarp>().getWorldMult();
		
		/*if (mCenter - mRange >= x) {
			mDirection *= -1;			
		} else if(mCenter + mRange <= x) {
			mDirection *= -1;
		}
		float y = gameObject.transform.position.y;
		float z = gameObject.transform.position.z;
		x += mDirection * mVelocity * Time.fixedDeltaTime * mMultiplier;
		gameObject.transform.position = new Vector3(x, y, z);
		*/
	//	rigidBody.velocity.x = mVelocity;
		if( x < mMinPosition || x>mMaxPosition){
			mVelocity = -1*mVelocity;
		}
		Rigidbody rigidBody = gameObject.GetComponent<Rigidbody>();
		rigidBody.velocity = new Vector3(mVelocity*mMultiplier,0,0);
	}
}
