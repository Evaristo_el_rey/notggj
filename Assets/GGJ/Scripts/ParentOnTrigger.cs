using UnityEngine;
using System.Collections;

public class ParentOnTrigger : MonoBehaviour {

	
	
	void OnTriggerEnter(Collider collider)
	{
	
		GameObject collisionObject = collider.gameObject;
		if( collisionObject.tag == "Player" )
		{
			collisionObject.transform.parent = gameObject.transform.parent;
		}
	
	}
	
	void OnTriggerExit(Collider collider)
	{
		GameObject collisionObject = collider.gameObject;
		if( collisionObject.tag == "Player" )
		{
			collisionObject.transform.parent = null;
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
