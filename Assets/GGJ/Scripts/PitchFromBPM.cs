using UnityEngine;
using System.Collections;

public class PitchFromBPM : MonoBehaviour {
	protected TimeWarp t;
	protected AudioSource so;
	void Start() { 
		t = GameObject.Find("singleton").GetComponent<TimeWarp>();
		so = this.GetComponent<AudioSource>();
	}
	// Update is called once per frame
	void Update () {
		so.pitch = t.getWorldMult();
	}
}
