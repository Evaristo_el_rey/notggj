using UnityEngine;
using System.Collections;

//Roca. Movimiento nulo excepto cuando el prota la toca.
//No puede moverse mas que en un nivel, nada de tirarla por barrancos.
public class ArrastrablePor : MonoBehaviour {
	public string tagBy="Player";
	protected bool protaMoving=false;
	protected Vector3 protaPos;
	protected Vector3 deltaPos;
	protected float originalZ;
	void Start() {
		originalZ = transform.position.z;
	}

	void FixedUpdate() {
		if(protaMoving) {
			print ("moving from "+this.transform.position);
			//this.transform.position += Time.fixedDeltaTime*protaPos;
			this.transform.position = new Vector3(protaPos.x + deltaPos.x, transform.position.y, originalZ);// + Time.fixedDeltaTime*protaPos;
			print ("moving to "+this.transform.position);
		}
	}
	void OnCollisionEnter(Collision coll) {
		OnTriggerEnter(coll.collider);
	}
	void OnTriggerEnter(Collider coll) {
		if(coll.tag==tagBy) {
			protaMoving=true;
			protaPos = coll.transform.position;
			deltaPos = transform.position - coll.transform.position;
		}
	}

	void OnCollisionStay(Collision coll) {
		OnTriggerStay(coll.collider);
	}
	void OnTriggerStay(Collider coll) {
		if(coll.tag==tagBy) {
			Vector3 movedPlayer = protaPos - coll.transform.position;
			//Si nos alejamos
			if(movedPlayer.x!=0 && (movedPlayer.x>0) == (deltaPos.x>0))
				protaMoving=false;
			protaPos = coll.transform.position;
		}
	}

	void OnCollisionExit(Collision coll) {
		OnTriggerExit(coll.collider);
	}
	void OnTriggerExit(Collider coll) {
		if(coll.tag==tagBy) {
			protaMoving=false;
		}
	}
}