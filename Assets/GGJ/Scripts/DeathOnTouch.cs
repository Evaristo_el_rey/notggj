using UnityEngine;
using System.Collections;

public class DeathOnTouch : MonoBehaviour {

	
	void OnCollisionEnter(Collision collision)
	//void OnControllerColliderHit(ControllerColliderHit hit)
	{
		OnTriggerEnter(collision.collider);
		//OnTriggerEnter(hit.collider);
	} 
	
	void OnTriggerEnter(Collider collider)
	{
		if(collider.gameObject.tag=="Player")
		{
			GameObject player = collider.gameObject;
			DeathController deathController = player.GetComponent<DeathController>();
			if(deathController==null)
			{
				print ("WARNING:DEATHONTOUCH: THE PLAYER DOES NOT HAVE A DEATH CONTROLLER.");
			}
			deathController.Die();
		}
	}
		
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
		void Update () {
	
	}
}
