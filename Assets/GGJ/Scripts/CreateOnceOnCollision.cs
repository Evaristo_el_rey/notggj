using UnityEngine;
using System.Collections;

public class CreateOnceOnCollision : MonoBehaviour {
	public GameObject CreateThis;
	public string OnCollisionWithName;
	protected bool created = false;

	void OnCollisionEnter(Collision collision) { OnTriggerEnter(collision.collider); }

	void OnTriggerEnter(Collider collider) {
		if(!created && collider.gameObject.name==OnCollisionWithName) {
			Instantiate(CreateThis, this.transform.position, Quaternion.identity);
			created=true;
		}
	}
}
