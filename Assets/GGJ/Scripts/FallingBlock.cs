using UnityEngine;
using System.Collections;

public class FallingBlock : MonoBehaviour 
{
	// Use this for initialization
	public float mVelocity = 1.5f;
	public float timeFallen = 1.0f;
	private float mMultiplier = 1.0f;
	private float mInitialY;
	private int mDirection = -1;
	private float elapsedTime = 0.0f;
	private float mInitialVelocity;
	void Start () 
	{
		mInitialY = gameObject.transform.position.y;
		mInitialVelocity = mVelocity;		
	}
	
	void FixedUpdate () 
	{
		mMultiplier =  GameObject.FindGameObjectWithTag("TimeWarp").GetComponent<TimeWarp>().getWorldMult();
		float x = gameObject.transform.position.x;
		float y = gameObject.transform.position.y;
		float z = gameObject.transform.position.z;
		
		if (y >= mInitialY) {
			mDirection *= -1;
		}
			
		if ((mDirection == 1 && y < mInitialY) || mDirection == -1) {
			float newY =  y + mDirection * mVelocity * mMultiplier * Time.fixedDeltaTime;
			gameObject.transform.position = new Vector3(x, newY, z);
		}
		
	}
	
	void OnCollisionEnter(Collision c)
	{
		if(c.gameObject.tag == "Floor")	{
			mDirection *= -1;
			mVelocity = 0.0f;
		}
	}
	
	void OnCollisionStay(Collision c)
	{
		if(c.gameObject.tag == "Floor")	{
			elapsedTime += Time.fixedDeltaTime;
			if (elapsedTime >= timeFallen) {
				mVelocity = mInitialVelocity;
				elapsedTime = 0.0f;
			}
		}
	}
	
}
