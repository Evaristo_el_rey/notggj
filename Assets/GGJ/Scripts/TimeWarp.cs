using UnityEngine;
using System.Collections;

public class TimeWarp : MonoBehaviour {
	
	public float mWorldMaxMultiplier = 1.5f;
	public float mWorldMinMultiplier = 0.5f;
	public float mPlayerMaxMultiplier = 1.5f;
	public float mPlayerMinMultiplier = 0.5f;	
	public float mPercentage = 1.0f;
	
	bool	mUp = true;
	
	// Use this for initialization
	void Start () {
		gameObject.tag = "TimeWarp";
	}
	
	//	0 < p <= 1 => player goes faster
	//	0 => player and world equally
	//	-1 <= p < 0 => player goes slower, world faster
	public void setPercentage ( float p )
	{
		if( p > 0 ) mUp = true; else mUp = false;
		mPercentage = Mathf.Abs(p);
	}
	
	// gives velocity multiplier for player
	public float getPlayerMult()
	{
		if(mUp) 
			return 1 + (mPlayerMaxMultiplier - 1.0f)*mPercentage;
		else
			return 1 + (1.0f - mPlayerMinMultiplier)*mPercentage;
	}
	
	// gives velocity multiplier for player
	public float getWorldMult()
	{
		if(mUp) 
			return 1 + (mWorldMaxMultiplier - 1.0f)*mPercentage;
		else
			return 1 + (1.0f - mWorldMinMultiplier)*mPercentage;
	}
}
